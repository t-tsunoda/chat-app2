/* eslint-disable */

// グローバル変数
let beforeDay

// クライアントからサーバーへの接続要求
const socket = io.connect()

// 読み込み時の処理
window.onload = function() {
  if(sessionStorage.getItem('loginUser')){
    const loginUser = sessionStorage.getItem('loginUser')
    getData('api/chatMessages',)
      .then(data => {
        for(let i=0; i<data.length; i++){
          const time = new Date(data[i].message_time)
          if(data[i].user === null){
            const userNull = '削除されたユーザー'
            const defaltIcon = 'https://cdn.aprico-media.com/production/imgs/images/000/008/511/original.jpg?1505866639'
            commentAdd(data[i].message_text,userNull,defaltIcon,time,data[i]._id,"")
          }else{
            commentAdd(data[i].message_text, data[i].user.user_name, data[i].user.user_icon,time,data[i]._id,data[i].user._id)
          }
        }
    })
  
    getData('api/users',)
      .then(data => {
        for(let i=0; i<data.length; i++){
          displayUserList(data[i].user_name,data[i].user_icon,data[i].user_isTeacher,data[i]._id)
          // ログインユーザー情報
          if(loginUser === data[i]._id){
            document.getElementById('login_user').innerHTML = data[i].user_name
            document.getElementById('login_icon').src = data[i].user_icon
          }
        }
    })
  }else{
    document.location.href = "index.html"
  }
   
}

// 接続時の処理
// socket.on('connect', () => {
//   console.log('connect')
// })

// Sendボタンを押した処理
const send = document.getElementById('send')
send.addEventListener('click',() => {
  sendMessage(sessionStorage.getItem('loginUser'))
  scrollBottom()
})

// Enterキーで送信処理
document.body.addEventListener('keydown', event => {
  if(event.key === 'Enter' && event.ctrlKey){
    sendMessage(sessionStorage.getItem('loginUser'))
  }
})

// メッセージ拡散時の処理
socket.on('spread message',chatId => {
  getData(`api/chatMessages/${chatId}`,).then(data => {
    const createTime = new Date(data.message_time)
    commentAdd(data.message_text, data.user.user_name, data.user.user_icon, createTime, data._id,data.user._id)
  })
})
// メッセージ削除拡散時の処理
socket.on('delete chat',chatId => {
  const selectChat = document.getElementById(chatId)
  if(selectChat=== null){
    location.reload()
  }
  deleteData(`api/chatMessages/${chatId}`,)
    .then(() => {
      selectChat.remove()
    })
})

// 新規ユーザー受信時
socket.on('new user',_id => {
  getData(`api/users/${_id}`,).then(data => {
    displayUserList(data.user_name,data.user_icon,data.user_isTeacher,data._id)
  })
})
// ユーザー削除拡散時の処理
socket.on('delete user',userId => {
  const selectUserHtml = document.getElementById(userId)
  deleteData(`api/users/${userId}`,)
    .then(() => {
      selectUserHtml.remove()
      if(userId === sessionStorage.getItem('loginUser')){
        document.location.href = "index.html"
      }
    })
})

