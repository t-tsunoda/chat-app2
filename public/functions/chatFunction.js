/* eslint-disable */

// チャット送信関数(post処理込み)
function sendMessage(loginUser){
    const input_text = document.getElementById('input-message')
    const msg = input_text.value
  
    if(msg.length !== 0){
      getData('api/users',).then(data => {
        for(let i = 0; i < data.length; i++) {
          if(loginUser === data[i]._id){
            postData('api/chatMessages', {
              user: data[i]._id,
              message_text: msg
            }).then(data => {
              socket.emit('new message',data._id)
              input_text.value = ''
              textAreaHeightSet()
            })
          }
        }
      })
    }
  }
  
  // チャット追加関数
  function commentAdd(Msg,userName,userIcon,time,chatId,userId){
    // 送信日時の取得
    const currentDate = DateFormat(new Date(time),'YYYY年MM月DD日')
    const currentTime = DateFormat(new Date(time),'HH:mm')
    const currentDayOfTheWeek = new Date(time).getDay()
    const dayOfTheWeek = [ '日', '月', '火', '水', '木', '金', '土' ][currentDayOfTheWeek]
  
    // コメント作成
    if(currentDate !== beforeDay){
      const createDay = '<div class="date-container">'
              + '<hr class="date-line"></hr>'
              + '<div class="date-box">'
              + '<div class="date">' + currentDate + '(' + dayOfTheWeek + ')</div>'
              + '</div>'
              + '</div>'
      document.getElementById('comment-main').insertAdjacentHTML('beforeend',createDay)
      beforeDay = currentDate
    }
    if(userId === sessionStorage.getItem('loginUser')){
      const createComment = '<div id="' + chatId + '" class="my-comment-block">'
            + '<div class="comment_container">'
            + '<div class="comment-time">  ' + currentTime + '</div>'
            + '<button onclick="DeleteConfirmeChat(event)" class="chatDelete" id="chatDelete" value=' + chatId + '>×</button>'
            + '<div class="my-comment-box">'
            + '<div class="comment">' + Msg + '</div>'
            + '</div>'
            + '</div>'
            + '</div>'
      document.getElementById('comment-main').insertAdjacentHTML('beforeend',createComment)
      scrollBottom()
    }else{
      const createComment = '<div id="' + chatId + '" class="comment-block">'
            + '<div class="comment-user">'
            + '<img class="user-icon" src=' + userIcon + ' alt="写真 ">'
            + '<div class="user-name">' + userName + '</div>'
            + '<div class="comment-time">  ' + currentTime + '</div>'
            + '</div>'
            + '<div class="comment-box">'
            + '<div class="comment">' + Msg + '</div>'
            + '</div>'
            + '</div>'
    document.getElementById('comment-main').insertAdjacentHTML('beforeend',createComment)
    scrollBottom()
    }
    
  }
  
  // チャット削除機能
  function chatDelete(e){
    const chatHtmlId = e.target.value
    socket.emit('delete chat', chatHtmlId)
    document.getElementById('chat_delete_screen').classList.toggle('openDeleteChat')
  }

  // Date format関数
  function DateFormat(date, format) {
    format = format.replace(/YYYY/, date.getFullYear())
    format = format.replace(/MM/, ('0' + (date.getMonth() + 1)).slice(-2))
    format = format.replace(/DD/, ('0' + date.getDate()).slice(-2))
    format = format.replace(/HH/,date.getHours())
    format = format.replace(/mm/,('0' + date.getMinutes()).slice(-2))
    return format
  }
  // テキストエリア行調整
  function textAreaHeightSet(){
    const input_text = document.getElementById('input-message')
    // 一旦テキストエリアを小さくしてスクロールバー（縦の長さを取得）
    input_text.style.height = '1px'
    const wSclollHeight = parseInt(input_text.scrollHeight)
  
    // テキストエリアの高さを設定する
    input_text.style.height = wSclollHeight + 'px'
  }
  
  //チャット自動スクロール ※commentAdd内で使用
  function scrollBottom(){
    const element = document.getElementById('comment-main')
    const scrollHeight = element.scrollHeight
    const y = element.scrollHeight - element.clientHeight
    element.scroll(0,y)
  }