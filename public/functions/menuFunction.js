/* eslint-disable */

// ドロップダウンメニュー開閉
function myFunction() {
    document.getElementById('myDropdown').classList.toggle('show')
  }
  // ドロップダウンメニュー画面外処理
  window.onclick = function(event) {
    if (!event.target.matches('.dropdown-icon')) {
      const dropdowns = document.getElementsByClassName('dropdown-container')
      let i
      for (i = 0; i < dropdowns.length; i++) {
        let openDropdown = dropdowns[i]
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show')
        }
      }
    }
  }
  
  // Menu開閉処理
  function MenuSwitch() {
    const input_user = document.getElementById('input_user')
    input_user.value = ''
    document.getElementById('user_add_screen').classList.toggle('open')
  }
  //Menu外選択時処理
  window.onclick = function(event) {
    if (event.target.matches('.user_add_screen')) {
      const addscreen = document.getElementsByClassName('user_add_screen')
      let i
      for (i = 0; i < addscreen.length; i++) {
        let open = addscreen[i]
        if (open.classList.contains('open')) {
          const input_user = document.getElementById('input_user')
          input_user.value = ''
          open.classList.remove('open')
        }
      }
    }
  }
  
  // ユーザー削除確認画面開閉処理
  function DeleteConfirmeUser(e) {
    if(e){
      getData(`api/users/${e.target.value}`,).then(data => {
        document.getElementById('user_delete_screen').classList.toggle('openDeleteUser')
        document.getElementById('delete_user').value = data._id
        document.getElementById('delete_user').innerHTML = data.user_name
        document.getElementById('delete_user_icon').src = data.user_icon
        
      })
    }else{
      document.getElementById('delete_user').innerHTML = ""
      document.getElementById('delete_user_icon').src = ""
      document.getElementById('user_delete_screen').classList.toggle('openDeleteUser')
    }
  }
  //削除確認画面外選択時処理
  window.onclick = function(event) {
    if (event.target.matches('.user_delete_screen')) {
      const deletescreen = document.getElementsByClassName('user_delete_screen')
      let i
      for (i = 0; i < deletescreen.length; i++) {
        let open = deletescreen[i]
        if (open.classList.contains('openDeleteUser')) {
          open.classList.remove('openDeleteUser')
        }
      }
    }
  }
  
  // チャット削除確認画面開閉処理
  function DeleteConfirmeChat(e) {
    if(e){
      getData(`api/chatMessages/${e.target.value}`,).then(data => {
        document.getElementById('chat_delete_screen').classList.toggle('openDeleteChat')
        document.getElementById('deleteChat').value = data._id
      })
    }else{
      document.getElementById('chat_delete_screen').classList.toggle('openDeleteChat')
    }
  }
  //削除確認画面外選択時処理
  window.onclick = function(event) {
    if (event.target.matches('.chat_delete_screen')) {
      const deletescreen = document.getElementsByClassName('chat_delete_screen')
      let i
      for (i = 0; i < deletescreen.length; i++) {
        let open = deletescreen[i]
        if (open.classList.contains('openDeleteChat')) {
          open.classList.remove('openDeleteChat')
        }
      }
    }
  }