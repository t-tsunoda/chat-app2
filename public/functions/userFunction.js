/* eslint-disable */

// ユーザー追加関数
function userAdd() {
    const input_user = document.getElementById('input_user')
    const newUser = input_user.value
    const input_category = document.getElementById('user_category').value
    let isTeacher
    if(input_category === 'teacher'){
      isTeacher = true
    }else{
      isTeacher = false
    }
    if(newUser.length !== 0){
      postData('api/users', {
        user_name: newUser ,
        user_icon: 'https://cdn.aprico-media.com/production/imgs/images/000/008/511/original.jpg?1505866639',
        user_isTeacher: isTeacher
      }).then(data => {
        socket.emit('new user',data._id)
        input_user.value = ''
        document.getElementById('user_add_screen').classList.toggle('open')
      })
      
    }
  }

  // ユーザー削除機能
  function userDelete(){
    const userId = document.getElementById('delete_user').value
    socket.emit('delete user',userId)
    document.getElementById('user_delete_screen').classList.toggle('openDeleteUser')
  }

  // ユーザー表示関数
  function displayUserList(userName,userIcon,isTeacher,id){
    if(isTeacher === true){
      const createTeacher = '<div id="' + id + '" class="user">'
              + '<img class="user-icon" src=' + userIcon +' alt="写真">'
              + '<div id="user_name" class="user-name">' + userName + '</div>'
              + '<button onclick="DeleteConfirmeUser(event)" class="userDelete" id="userDelete" value=' + id + '>×</button>'
              + '</div>'
      document.getElementById('teacher_container').insertAdjacentHTML('beforeend',createTeacher)
    }else{
      const createStudent = '<div id="' + id + '" class="user">'
              + '<img class="user-icon" src=' + userIcon +' alt="写真">'
              + '<div id="user_name" class="user-name">' + userName + '</div>'
              + '<button onclick="DeleteConfirmeUser(event)" class="userDelete" id="userDelete" value=' + id + '>×</button>'
              + '</div>'
      document.getElementById('user_add').insertAdjacentHTML('beforebegin',createStudent)
    }
  
  }

   // ログアウト処理
   function logout() {
    sessionStorage.removeItem('loginUser')
    document.location.href = "index.html"
  }