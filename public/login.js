/* eslint-disable */

// 読み込み時の処理
window.onload = function() {
  if(sessionStorage.getItem('loginUser')){
    sessionStorage.removeItem('loginUser')
  }
  getData('api/users',)
      .then(data => {
        for(let i=0; i<data.length; i++){
          setUser(data[i]._id,data[i].user_name)
        }
      })
  }

// ログインボタン押下時
const login = document.getElementById('login')
login.addEventListener('click',() => {
    const selectedUser = document.getElementById('select_user').value
    getData('api/users',)
      .then(data => {
        for(let i=0; i<data.length; i++){
          if(data[i]._id === selectedUser){
            sessionStorage.setItem('loginUser',selectedUser)
            document.location.href = "chat.html"
          }
        }
    })
})


